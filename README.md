This package exists to quickly and easily create a breadcrumb
for your current Vue route.

## Requirements
The `vue-async-computed` plugin must be installed.

## Installation
`npm install vue-router-bootstrap-breadcumbs`

## Usage
`import breadcrumbs from 'vue-router-bootstrap-breadcrumbs';`

`<breadcrumbs></breadcrumbs>`

## Options
`:home="true"` allows you to show a link to the root (`/`) of your
app. By default it does not appear.

By setting a `breadcrumb` computed property on a route component,
you can change the text that appears in the breadcrumb.

`:opt_in="true"` allows you to show breadcrumbs _only_ for those components
that specify a `breadcrumb` computed property.
